# Flight Tools

Various tools for calculating flight data,
such as crosswind, head/tailwind, and more.
My brother wo wants to become a pilot asked for these,
so I figured i'd upload them here if anyone else needs them.

## Install

Go to [Releases](https://gitlab.com/faden/flight-tools/-/tags) and download the package for your operating system.
The software is developed on Arch based Linux, but should work on Windows.

## Build

To build the project, open <project.godot> in Godot (3.0+) and export
