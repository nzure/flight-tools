extends Control

class_name Polar

export (int) var radius: int = 100
export (bool) var relative: bool = true
export (int) var steps: int = 5
export (int) var sectors: int = 12

var center: Vector2 = Vector2(0,0)
var detail: int = 100
var minimumSize: Vector2 = Vector2(250,250)

func _ready() -> void:
    set_process(true)
    set_custom_minimum_size(minimumSize)
    get_tree().root.connect("size_changed", self, "_onResize")
    _onResize()

func _onResize():
    center = rect_size / 2

    if relative:
        radius = min(rect_size.x, rect_size.y)
        print(radius)
        radius = radius / 3
    update()


func _draw() -> void:
    var step = floor(radius / steps as float)

    #Circles
    for i in range(0, radius, step):
        draw_arc(center, i, 0, 2*PI, detail, Palenight.lightBlue, 1.5, true)
    draw_arc(center, radius, 0 ,2*PI, detail, Palenight.blue, 3, true)

    # Sectors and Labels
    var angle = 360.0 / sectors
    for i in range(0, 360, angle):
        var line = polar2cartesian(radius * 1, deg2rad(i - 90)) + center
        draw_line(line, center, Palenight.text, 1, true)

        var point = polar2cartesian((radius * 1) + 20, deg2rad(i - 90)) + center
        var fontSize = Palenight.font.get_string_size(i as String)
        point.x -= fontSize.x / 2
        point.y += fontSize.y / 4
        draw_string(Palenight.font, point, i as String, Palenight.textAlt, -1)

